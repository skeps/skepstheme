<?php
/**
 * Add settings page with global option fields via ACF
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */


if( function_exists('acf_add_options_page') ) {
    
       $option_page = acf_add_options_page(array(
           'page_title' 	=> 'Site instellingen',
           'menu_title' 	=> 'Site instellingen',
           'menu_slug' 	=> 'theme-general-settings',
           'capability' 	=> 'edit_posts',
           'redirect' 	=> false
       ));

        acf_add_options_sub_page(array(
        'page_title' 	=> 'Contactgegevens',
        'menu_title' 	=> 'Contactgegevens',
        'parent_slug' 	=> $option_page['menu_slug'],
        ));

        acf_add_options_sub_page(array(
            'page_title' 	=> 'Openingstijden',
            'menu_title' 	=> 'Openingstijden',
            'parent_slug' 	=> $option_page['menu_slug'],
        ));
    
}


function gmap_acf() {
	
    acf_update_setting('google_api_key', 'AIzaSyBMyxVwfFI9abHxAbqVq0eC-KKW70V43p0');
    
}

function remove_gravityforms_style() {
	wp_dequeue_style('gforms_css');
}

add_action('wp_print_styles', 'remove_gravityforms_style');
add_action('acf/init', 'gmap_acf');