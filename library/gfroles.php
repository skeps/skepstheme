<?php
/**
 * Add all Gravity Forms capabilities to Editor role.
 * Runs during plugin activation.
 *
 * @access public
 * @return void
 */
function activate_skepsgfroles() {

  $role = get_role( 'editor' );
  $role->add_cap( 'gravityforms_delete_entries' );
  $role->add_cap( 'gravityforms_edit_entries' );
  $role->add_cap( 'gravityforms_edit_entry_notes' );
  $role->add_cap( 'gravityforms_edit_forms' );
  $role->add_cap( 'gravityforms_export_entries' );
  $role->add_cap( 'gravityforms_view_entries' );
  $role->add_cap( 'gravityforms_view_entry_notes' );

}
// Register our activation hook
register_activation_hook( __FILE__, 'activate_skepsgfroles' );

/**
 * Remove Gravity Forms capabilities from Editor role.
 * Runs during plugin deactivation.
 *
 * @access public
 * @return void
 */
function deactivate_skepsgfroles() {

  $role = get_role( 'editor' );
   $role->remove_cap( 'gravityforms_delete_entries' );
   $role->remove_cap( 'gravityforms_edit_entries' );
   $role->remove_cap( 'gravityforms_edit_entry_notes' );
   $role->remove_cap( 'gravityforms_edit_forms' );
   $role->remove_cap( 'gravityforms_export_entries' );
   $role->remove_cap( 'gravityforms_view_entries' );
   $role->remove_cap( 'gravityforms_view_entry_notes' );
}
// Register our de-activation hook
register_deactivation_hook( __FILE__, 'deactivate_skepsgfroles' );
?>
