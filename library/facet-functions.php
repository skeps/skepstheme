<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


//FACETWP Functions

add_filter( 'facetwp_result_count', function( $output, $params ) {

    $countposts = wp_count_posts('vacancy');

    $output = '<div class="result-count">';
    $output .= '<span class="count">' . $params['total'] . '</span> <span> van de </span> <span class="count">'.$countposts->publish.'</span> vacatures gevonden';
    $output .= '</div>';
    return $output;
}, 10, 2 );

function custom_facetwp_pager( $output, $params ) {
    $output = '';
    $page = (int) $params['page'];
    $total_pages = (int) $params['total_pages'];
    // Only show pagination when > 1 page
    if ( 1 < $total_pages ) {
        if ( 1 < $page ) {
            $output .= '<li><a class="facetwp-page" data-page="' . ( $page - 1 ) . '"><i class="fa fa-angle-left"></i></a></li>';
        }
        if ( 3 < $page ) {
            $output .= '<li><a class="facetwp-page first-page" data-page="1">1</a></li>';
            $output .= ' <span class="dots">…</span> ';
        }
        for ( $i = 2; $i > 0; $i-- ) {
            if ( 0 < ( $page - $i ) ) {
                $output .= '<li><a class="facetwp-page" data-page="' . ($page - $i) . '">' . ($page - $i) . '</a></li>';
            }
        }
        // Current page
        $output .= '<li><a class="facetwp-page active" data-page="' . $page . '">' . $page . '</a></li>';
        for ( $i = 1; $i <= 2; $i++ ) {
            if ( $total_pages >= ( $page + $i ) ) {
                $output .= '<li><a class="facetwp-page" data-page="' . ($page + $i) . '">' . ($page + $i) . '</a></li>';
            }
        }
        if ( $total_pages > ( $page + 2 ) ) {
            $output .= ' <span class="dots">…</span> ';
            $output .= '<li><a class="facetwp-page last-page" data-page="' . $total_pages . '">' . $total_pages . '</a></li>';
        }
        if ( $page < $total_pages ) {
            $output .= '<li><a class="facetwp-page" data-page="' . ( $page + 1 ) . '"><i class="fa fa-angle-right"></i></a></li>';
        }
    }
    return $output;
}
add_filter( 'facetwp_pager_html', 'custom_facetwp_pager', 10, 2 );

?>
