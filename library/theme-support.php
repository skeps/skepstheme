<?php
/**
 * Register theme support for languages, menus, post-thumbnails, post-formats etc.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_theme_support' ) ) :
function foundationpress_theme_support() {
	// Add language support
	load_theme_textdomain( 'foundationpress', get_template_directory() . '/languages' );

	// Switch default core markup for search form, comment form, and comments to output valid HTML5
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Add menu support
	add_theme_support( 'menus' );

	// Let WordPress manage the document title
	add_theme_support( 'title-tag' );

	// Add post thumbnail support: http://codex.wordpress.org/Post_Thumbnails
	add_theme_support( 'post-thumbnails' );

	// RSS thingy
	add_theme_support( 'automatic-feed-links' );

	// Add post formats support: http://codex.wordpress.org/Post_Formats
	add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat') );

	// Additional theme support for woocommerce 3.0.+
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );

	// Add foundation.css as editor style https://codex.wordpress.org/Editor_Style
	add_editor_style( 'dist/assets/css/' . foundationpress_asset_path('app.css'));

	add_action('init', 'modifySeperator');
	function modifySeperator() {
		$bcnoptions = get_option('bcn_options');
		$newoptions = str_replace("&gt;","",$bcnoptions);
		update_option('bcn_options', $newoptions);
	}

}

add_action( 'after_setup_theme', 'foundationpress_theme_support' );
endif;

// Wordpress login logo
function my_login_logo() {

	$logo = get_field('wp_login_logo', 'options');
	$url = $logo['sizes']['medium'];
	$width = $logo['sizes']['medium-width'] . 'px';
	$height = $logo['sizes']['medium-height'] . 'px';

	?>

    <style type="text/css">
        #login h1 a, .login h1 a {
          background-image: url('<?= $url; ?>');
					height: <?= $height; ?>;
					width: <?= $width; ?>;
					background-size: contain;
					background-position: center center;
					background-repeat: no-repeat;
      		padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// editor can edit theme

// get the the role object
$role_object = get_role( 'editor' );

// add $cap capability to this role object
$role_object->add_cap( 'edit_theme_options' );

// For long forms
add_filter( 'gform_confirmation_anchor', '__return_true' );
