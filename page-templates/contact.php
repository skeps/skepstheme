<?php
/*
Template Name: Contactpagina
*/
get_header(); ?>

<article class="">
  <div class="entry-content">
    <?php 
    $monday = get_field( 'monday', 'option' );
    $tuesday = get_field( 'tuesday', 'option' );
    $wednesday = get_field( 'wednesday', 'option' );
    $thursday = get_field( 'thursday', 'option' );
    $friday = get_field( 'friday', 'option' );
    $saturday = get_field( 'saturday', 'option' );
    $sunday = get_field( 'sunday', 'option' );
    ?>
        <table class="contact unstriped">

            <tbody>
            <?php if ($monday['opened'] || $monday['closed']): ?>

                <tr>
                    <th>Maandag</th>
                    <?php if ($monday['gesloten'] == 0): ?>
                      <td>
                          <?= $monday['opened']; ?> - <?= $monday['closed']; ?>
                      </td>
                    <?php else: ?>
                      <td>
                        Gesloten
                      </td>
                    <?php endif; ?>
                </tr>
              <?php endif; ?>
              <?php if ($tuesday['opened'] || $tuesday['closed']): ?>
                <tr>
                  <th>Dinsdag</th>
                  <?php if ($tuesday['gesloten'] == 0): ?>
                    <td>
                        <?= $tuesday['opened']; ?> - <?= $tuesday['closed']; ?>
                    </td>
                  <?php else: ?>
                    <td>
                      Gesloten
                    </td>
                  <?php endif; ?>
                </tr>
              <?php endif; ?>
              <?php if ($wednesday['opened'] || $wednesday['closed']): ?>
                <tr>
                  <th>Woensdag</th>
                  <?php if ($wednesday['gesloten'] == 0): ?>
                    <td><?= $wednesday['opened']; ?> - <?= $wednesday['closed']; ?></td>
                  <?php else: ?>
                    <td>
                      Gesloten
                    </td>
                  <?php endif; ?>
                </tr>
              <?php endif; ?>
              <?php if ($thursday['opened'] || $thursday['closed']): ?>
                <tr>
                  <th>Donderdag</th>
                  <?php if ($thursday['gesloten'] == 0): ?>
                    <td><?= $thursday['opened']; ?> - <?= $thursday['closed']; ?></td>
                  <?php else: ?>
                    <td>
                      Gesloten
                    </td>
                  <?php endif; ?>
                </tr>
              <?php endif; ?>
              <?php if ($friday['opened'] || $friday['closed']): ?>
                <tr>
                  <th>Vrijdag</th>
                  <?php if ($friday['gesloten'] == 0): ?>
                    <td><?= $friday['opened']; ?> - <?= $friday['closed']; ?></td>
                  <?php else: ?>
                    <td>
                      Gesloten
                    </td>
                  <?php endif; ?>
                </tr>
              <?php endif; ?>
              <?php if ($saturday['opened'] || $saturday['closed']): ?>
                <tr>
                  <th>Zaterdag</th>
                  <?php if ($saturday['gesloten'] == 0): ?>
                    <td><?= $saturday['opened']; ?> - <?= $saturday['closed']; ?></td>
                <?php else: ?>
                  <td>
                    Gesloten
                  </td>
                <?php endif; ?>
                </tr>
              <?php endif; ?>
              <?php if ($sunday['opened'] || $sunday['closed']): ?>
                <tr>
                  <th>Zondag</th>
                  <?php if ($sunday['gesloten'] == 0): ?>
                    <td><?= $sunday['opened']; ?> - <?= $sunday['closed']; ?></td>
                <?php else: ?>
                  <td>
                    Gesloten
                  </td>
                <?php endif; ?>
                </tr>
              <?php endif; ?>

            </tbody>

        </table>
  </div>
</article>

<?php get_footer();
