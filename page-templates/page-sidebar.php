<?php
/*
Template Name: Pagina met zijbalk
*/
get_header(); ?>

<?php get_template_part( 'template-parts/heros/featured-image' ); ?>

<?php $sidebarposition = (get_field('position_sidebar')) ? 'sidebar-left' : ''; ?>
<?php $sidebarsticky = (get_field('sticky_sidebar')) ? true : false; ?>

<div class="main-wrap <?= $sidebarposition; ?>">

	<main class="main-content" id="maincontent">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'template-parts/objects/pages/content-page' ); ?>
			<?php get_template_part('template-parts/call-to-actions/social-share'); ?>
			<?php comments_template(); ?>
		<?php endwhile;?>
	 </main>

	 <?php if($sidebarsticky) : ?>
		 <div class="sidebar" data-sticky-container>
			  <div class="sticky" data-sticky data-anchor="maincontent">
					<?php get_sidebar(); ?>
				</div>
			</div>
	 <?php else : ?>
		 <div class="sidebar">
			 <?php get_sidebar(); ?>
		 </div>
	 <?php endif; ?>


</div>
<?php get_footer();
