<?php
/*
Template Name: Volledige breedte
*/
get_header(); ?>

<?php get_template_part( 'template-parts/heros/featured-image' ); ?>

<div class="main-wrap full-width">
	<main class="main-content">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'template-parts/objects/pages/content-page' ); ?>
			<?php comments_template(); ?>
		<?php endwhile;?>
	</main>
</div>
<?php get_footer();
