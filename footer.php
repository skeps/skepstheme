<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

</div><!-- Close container -->
	<div class="footer-container">
		<footer class="footer">
			<?php dynamic_sidebar( 'footer-widgets' ); ?>
		</footer>
	</div>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	</div><!-- Close off-canvas content -->
<?php endif; ?>

<?php get_template_part('template-parts/modals/modal'); ?>

<?php wp_footer(); ?>

<?php $enabled = (get_field('cookiealert_status', 'option')) ? 'true' : 'false'; ?>
<script type="text/javascript">
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
		"position": "top",
		"static": true,
		"cookie.expiryDays": 1,
		"enabled": <?= $enabled ?>,
		"content": {
			"message": app.cookiemessage,
			"dismiss": app.cookiedismiss,
			"link": app.cookielink,
			"href": app.cookiehref
		}
	})});
</script>
</body>
</html>
