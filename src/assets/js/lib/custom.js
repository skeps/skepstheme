// Trigger function for modals; fixes bug in Foundation
// Triggers on attribute 'data-modal'
// Set modal id the same as the attribute
$(document).ready(function(){
  $('[data-modal]').each(function(){

    $(this).on('click', function(){
      var modal = $(this).data('modal');
      $('#'+modal).foundation('open');
    });

  });
});
