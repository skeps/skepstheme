// Documentation https://xdsoft.net/jqplugins/datetimepicker/
// Initialized on attributes 'date-picker', 'time-picker' or 'datetime-picker'
$.datetimepicker.setLocale('nl');
jQuery(document).ready(function(){
  $('[date-picker]').datetimepicker({
    timepicker:false
  });
  $('[time-picker]').datetimepicker({
    datepicker:false
  });
  $('[datetime-picker]').datetimepicker();
});
