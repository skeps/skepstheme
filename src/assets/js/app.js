import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

import '../../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox';
import '../../../node_modules/owl.carousel/dist/owl.carousel.min.js';
import '../../../node_modules/jquery-datetimepicker/build/jquery.datetimepicker.full.js';
import './lib/cookieconsent.min';


import './lib/custom';
import './lib/fancybox';
import './lib/datetimepicker';
import './lib/sliders';

$(document).foundation();
