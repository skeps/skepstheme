<?php if(function_exists('bcn_display')) {
  echo '<nav aria-label="You are here:" role="navigation">';
  echo "<div class='breadcrumbs'>";
    bcn_display();
  echo '</div>';
  echo '</nav>';
} ?>
