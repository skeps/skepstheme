<div class="cta social-share">
  <div class="left">
    <span><?php _e('Delen op: ', 'skepstheme') ?></span>
  </div>
  <div class="right">
    <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share on Facebook." onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
      <div class="icon-container facebook">
        <i class="fa fa-facebook" aria-hidden="true"></i>
      </div>
    </a>
    <a href="https://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" title="Tweet this!" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
      <div class="icon-container twitter">
        <i class="fa fa-twitter" aria-hidden="true"></i>
      </div>
    </a>
    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" title="Share on LinkedIn" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
      <div class="icon-container linkedin">
        <i class="fa fa-linkedin" aria-hidden="true"></i>
      </div>
    </a>
    <a href="mailto:?subject=<?php the_title() ?>&amp;body= <?php the_permalink(); ?>">
      <div class="icon-container mail">
        <i class="fa fa-envelope-o" aria-hidden="true"></i>
      </div>
    </a>
    <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
      <div class="icon-container google">
        <i class="fa fa-google-plus" aria-hidden="true"></i>
      </div>
    </a>
    <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&description=<?php the_title(); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
      <div class="icon-container google">
        <i class="fa fa-pinterest" aria-hidden="true"></i>
      </div>
    </a>
  </div>
</div>
